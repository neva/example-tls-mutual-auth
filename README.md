
# Table of Contents

1.  [Mutual Authentication using TLS](#orga603297)
    1.  [CA](#org5567ba3)
        1.  [Installation](#orgfbbc0a0)
        2.  [Set up certificates](#org6c08720)
    2.  [Server component](#orgdc174bc)
    3.  [Client component](#org80024cf)
    4.  [Demonstrating mutual auth](#org895b26a)
    5.  [Testing no client certificate](#org11a4a4b)


<a id="orga603297"></a>

# Mutual Authentication using TLS

For mutual auth, you need a certificate authority. For this example we will create our own CA.

<a id="org5567ba3"></a>

## CA

<a id="orgfbbc0a0"></a>

### Installation

<https://smallstep.com/docs/step-cli/installation>

``` sh
brew install step
```

we'll set up a password protected certificate with the password of `example`

``` sh
step ca init
```

``` text
✔ Deployment Type: Standalone
What would you like to name your new PKI?
✔ (e.g. Smallstep): Smallstep
What DNS names or IP addresses would you like to add to your new CA?
✔ (e.g. ca.smallstep.com[,1.1.1.1,etc.]): localhost,127.0.0.1
What IP and port will your new CA bind to?
✔ (e.g. :443 or 127.0.0.1:4343): 127.0.0.1:14343
What would you like to name the CA&rsquo;s first provisioner?
✔ (e.g. you@smallstep.com): ca-example@example.app
Choose a password for your CA keys and first provisioner.
✔ [leave empty and we&rsquo;ll generate one]:

Generating root certificate&#x2026; done!
Generating intermediate certificate&#x2026; done!

✔ Root certificate: *Users/ejohnson1*.step/certs/root<sub>ca.crt</sub>
✔ Root private key: *Users/ejohnson1*.step/secrets/root<sub>ca</sub><sub>key</sub>
✔ Root fingerprint: 123ae7987c0dbdea85d0e9533ebab8675115cda056a669fbac3adcac03d71d5c
✔ Intermediate certificate: *Users/ejohnson1*.step/certs/intermediate<sub>ca.crt</sub>
✔ Intermediate private key: *Users/ejohnson1*.step/secrets/intermediate<sub>ca</sub><sub>key</sub>
✔ Database folder: *Users/ejohnson1*.step/db
✔ Default configuration: *Users/ejohnson1*.step/config/defaults.json
✔ Certificate Authority configuration: *Users/ejohnson1*.step/config/ca.json

Your PKI is ready to go. To generate certificates for individual services see &rsquo;step help ca&rsquo;.

FEEDBACK 😍 🍻
  The step utility is not instrumented for usage statistics. It does not phone
  home. But your feedback is extremely valuable. Any information you can provide
  regarding how you’re using \`step\` helps. Please send us a sentence or two,
  good or bad at feedback@smallstep.com or join GitHub Discussions
  <https://github.com/smallstep/certificates/discussions> and our Discord
  <https://u.step.sm/discord>.
```

Start the CA (run CA as server so our client and server can validate certificates against it)

``` sh
step-ca $(step path)/config/ca.json
```

<a id="org6c08720"></a>

### Set up certificates

``` sh
    step ca certificate "localhost" server.crt server.key
```

``` text
✔ Provisioner: ca-example@example.app (JWK) [kid: QNInyqJrYcvNZe9xrMgXb7f4XKKZEBtWXiCUsX2sgcs]
✔ Please enter the password to decrypt the provisioner key:
✔ CA: <https://localhost:14343>
✔ Certificate: server.crt
✔ Private Key: server.key

Get a copy of the root CA certs

    step ca root ca.crt

The root certificate has been saved in ca.crt.
```


<a id="orgdc174bc"></a>

## Server component

We&rsquo;ll start a server and have it use the server certificates. Note that this does not need to be an HTTPS server. The `tls` component could be used instead to encapsulate any protocol. HTTPS uses `tls` and `http` under the hood.

In this example we&rsquo;re not validating the client certificate.

``` javascript
    // file:server-noca.js
    const fs = require('fs');
    const https = require('https');
    const cert = fs.readFileSync('server.crt');
    const key = fs.readFileSync('server.key');
    const options = { cert, key }
    const handler = (req, res) => {
        res.writeHead(200);
        res.end('Hello, world!');
    };
    
    https.createServer(options, handler).listen(9443);
```

If we want to validate the client, we&rsquo;ll include CA and request the certificate from the client for verification. We&rsquo;ll handled the rejection in the handler rather than allowing the server component to reject them.

``` javascript
    // file:server-ca.js
    const fs = require('fs');
    const https = require('https');
    const requestCert = true;
    const rejectUnauthorized = false;
    const ca = fs.readFileSync('ca.crt');
    const cert = fs.readFileSync('server.crt');
    const key = fs.readFileSync('server.key');
    const options = { requestCert, rejectUnauthorized, ca, cert, key };
    const handler = (req, res) => {
        if(!req.client.authorized) {
            res.writeHead(401);
            return res.end('Invalid client certificate');
        }
        res.writeHead(200);
        res.end('Hello, world!');
    };
    
    https.createServer(options, handler).listen(9443);
```


<a id="org80024cf"></a>

## Client component

On the client side we&rsquo;ll want to generate a certificate for it to use.

``` sh
    step ca certificate "client" client.crt client.key
```

We should already have the ca.crt available for use on the client, but it is needed as well.

``` javascript
    // file:client-ca.js
    const fs = require('fs');
    const https = require('https');
    const { hostname, port, path } = new URL('https://localhost:9443/');
    const method = 'GET';
    const ca = fs.readFileSync('ca.crt');
    const cert = fs.readFileSync('client.crt');
    const key = fs.readFileSync('client.key');
    const options = { hostname, port, path, method, ca, cert, key };
    const handler = (res) => {
        res.on('data', (data) => process.stdout.write(data));
    };
    const req = https.request(options, handler);
    
    req.end();
```


<a id="org11a4a4b"></a>

## Testing no client certificate

To test without a client cert, we modify the client to not send the certificate, and use the system default.

``` javascript
    // file:client-noca.js
    const fs = require('fs');
    const https = require('https');
    const { hostname, port, path } = new URL('https://localhost:9443/');
    const method = 'GET';
    const options = { hostname, port, path, method };
    const handler = (res) => {
        res.on('data', (data) => process.stdout.write(data));
    };
    const req = https.request(options, handler);
    
    req.end();
```

``` sh
    node client-noca.js
``` 

``` text
node:events:346
      throw er; // Unhandled &rsquo;error&rsquo; event
      ^

Error: unable to get local issuer certificate
    at TLSSocket.onConnectSecure (node:<sub>tls</sub><sub>wrap</sub>:1531:34)
    at TLSSocket.emit (node:events:369:20)
    at TLSSocket.<sub>finishInit</sub> (node:<sub>tls</sub><sub>wrap</sub>:945:8)
    at TLSWrap.ssl.onhandshakedone (node:<sub>tls</sub><sub>wrap</sub>:719:12)
Emitted &rsquo;error&rsquo; event on ClientRequest instance at:
    at TLSSocket.socketErrorListener (node:<sub>http</sub><sub>client</sub>:469:9)
    at TLSSocket.emit (node:events:369:20)
    at emitErrorNT (node:internal/streams/destroy:188:8)
    at emitErrorCloseNT (node:internal/streams/destroy:153:3)
    at processTicksAndRejections (node:internal/process/task<sub>queues</sub>:81:21) {
  code: &rsquo;UNABLE<sub>TO</sub><sub>GET</sub><sub>ISSUER</sub><sub>CERT</sub><sub>LOCALLY</sub>&rsquo;
}
```

The client will not connect because it does not have the CA of the server. We&rsquo;ll modify the code so it **does** have the CA, but does not send a client cert:

``` javascript
    // file:client-nocert.js
    const fs = require('fs');
    const https = require('https');
    const ca = fs.readFileSync('ca.crt');
    const { hostname, port, path } = new URL('https://localhost:9443/');
    const method = 'GET';
    const options = { hostname, port, path, method, ca };
    const handler = (res) => {
        res.on('data', (data) => process.stdout.write(data));
    };
    const req = https.request(options, handler);
    
    req.end();
```

``` sh
    node client-nocert.js
```

We connect successfully, but the server rejects the certificate from the system since it was not signed by our CA.



<a id="org895b26a"></a>

## Demonstrating mutual auth

Now with the `server-ca.js` and the `client-ca.js` we can run them together to prove they both work. The client won&rsquo;t connect if the server&rsquo;s certificate is invalid by default, and with our server code, it will validate the client is who they say they are using certificate auth.

Start the server:

``` sh
    node server-ca.js
```
Run the client:

``` sh
    node client-ca.js
```

Our server properly responds with `Hello, world!` and all certificates are valid.

This fully demonstrates TLS and HTTPS&rsquo;s ability to verify both server and client certificates through mutual authentication.
