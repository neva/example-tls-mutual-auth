const fs = require('fs');
const https = require('https');
const { hostname, port, path } = new URL('https://localhost:9443/');
const method = 'GET';
const ca = fs.readFileSync('ca.crt');
const cert = fs.readFileSync('client.crt');
const key = fs.readFileSync('client.key');
const options = { hostname, port, path, method, ca, cert, key };
const handler = (res) => {
    res.on('data', (data) => process.stdout.write(data));
};
const req = https.request(options, handler);

req.end();
