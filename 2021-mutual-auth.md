
# Table of Contents

1.  [Mutual Authentication using TLS](#orga603297)
    1.  [CA](#org5567ba3)
        1.  [Installation](#orgfbbc0a0)
        2.  [Set up certificates](#org6c08720)
    2.  [Server component](#orgdc174bc)
    3.  [Client component](#org80024cf)
    4.  [Demonstrating mutual auth](#org895b26a)
    5.  [Testing no client certificate](#org11a4a4b)


<a id="orga603297"></a>

# Mutual Authentication using TLS

For mutual auth, you need a certificate authority. For this example we will create our own CA.


<a id="org5567ba3"></a>

## CA


<a id="orgfbbc0a0"></a>

### Installation

<https://smallstep.com/docs/step-cli/installation>

    brew install step

Testing the install

    step certificate inspect https://smallstep.com

    Certificate:
        Data:
            Version: 3 (0x2)
            Serial Number: 390355752489150034894579713154517604864320 (0x47b270744ad1f3f654adca74ad7f7b99140)
        Signature Algorithm: SHA256-RSA
            Issuer: C=US,O=Let's Encrypt,CN=R3
            Validity
                Not Before: Aug 10 07:39:23 2021 UTC
                Not After : Nov 8 07:39:21 2021 UTC
            Subject: CN=*.smallstep.com
            Subject Public Key Info:
                Public Key Algorithm: ECDSA
                    Public-Key: (256 bit)
                    X:
                        84:ef:9b:58:8d:88:36:7a:51:4f:a2:a0:72:74:dc:
                        18:76:c7:b3:21:06:c8:51:8c:6b:b8:72:8f:05:1e:
                        66:22
                    Y:
                        cf:3e:ca:7b:06:96:58:94:c8:43:bd:3e:53:40:eb:
                        2f:09:4d:1e:3f:e4:08:46:e7:1b:7f:7f:0a:68:87:
                        1f:b4
                    Curve: P-256
            X509v3 extensions:
                X509v3 Key Usage: critical
                    Digital Signature
                X509v3 Extended Key Usage:
                    Server Authentication, Client Authentication
                X509v3 Basic Constraints: critical
                    CA:FALSE
                X509v3 Subject Key Identifier:
                    C2:77:24:31:13:B7:B3:E7:E8:C4:45:6A:D7:B0:F3:DD:62:EE:75:9D
                X509v3 Authority Key Identifier:
                    keyid:14:2E:B3:17:B7:58:56:CB:AE:50:09:40:E6:1F:AF:9D:8B:14:C2:C6
                Authority Information Access:
                    OCSP - URI:http://r3.o.lencr.org
                    CA Issuers - URI:http://r3.i.lencr.org/
                X509v3 Subject Alternative Name:
                    DNS:*.smallstep.com, DNS:smallstep.com
                X509v3 Certificate Policies:
                    Policy: 2.23.140.1.2.1
                    Policy: 1.3.6.1.4.1.44947.1.1.1
                RFC6962 Certificate Transparency SCT:
                    SCT [0]:
                        Version: V1 (0x0)
                        LogID: RJRlLrDuzq/EQAfYqP4owNrmgr7YyzG1P9MzlrW2gag=
                        Timestamp: Aug 10 08:39:23.836 2021 UTC
                        Signature Algorithm: SHA256-ECDSA
                          30:46:02:21:00:a6:81:14:13:5b:fd:49:bd:ee:72:09:2b:bb:
                          2d:b3:68:84:49:13:47:b9:24:25:69:60:91:48:c2:3c:b6:16:
                          03:02:21:00:d3:4a:94:b4:99:94:25:1e:c3:d6:0e:c5:62:24:
                          ba:9b:e1:a6:2b:62:93:23:8b:4b:fd:35:d1:31:af:b5:8f:f4
                    SCT [1]:
                        Version: V1 (0x0)
                        LogID: fT7y+I//iFVoJMLAyp5SiXkrxQ54CX8uapdomX4i8Nc=
                        Timestamp: Aug 10 08:39:23.866 2021 UTC
                        Signature Algorithm: SHA256-ECDSA
                          30:45:02:21:00:d5:5a:d5:f0:a8:bd:8e:76:14:50:aa:1c:c4:
                          37:e7:2e:c2:4c:09:ea:a0:8d:64:e0:bb:91:1b:1b:06:b2:1b:
                          f3:02:20:52:4a:69:a5:99:c4:df:f0:98:ee:ed:69:84:cf:dc:
                          bf:44:27:0e:b8:0b:32:40:c5:4a:42:f7:3b:f1:1b:9c:4e
        Signature Algorithm: SHA256-RSA
             94:ce:15:2f:a3:6d:be:2d:e7:1a:3c:4a:f0:1c:c9:8c:66:59:
             cd:de:e7:fb:cb:43:31:2a:45:0a:9b:66:30:2a:b2:8d:4b:60:
             d8:ea:ba:59:46:8b:97:f7:81:a7:3c:da:05:c4:14:8f:9d:d5:
             b3:6b:b8:c2:17:e2:ae:59:af:02:33:10:ca:75:b8:3f:e3:8f:
             ec:f2:00:42:9f:9c:2b:28:a4:a4:26:8e:85:ac:47:69:31:84:
             ce:f1:25:a1:2f:60:d4:5d:f5:68:d4:9e:84:bf:8c:71:7e:82:
             3b:e4:e3:23:cc:56:6f:cc:bb:e6:61:e1:12:54:70:c8:03:0e:
             da:e6:3a:dd:08:a4:68:95:d0:80:aa:a5:a3:1e:85:f6:f3:7e:
             3b:d6:a9:7e:da:76:45:c4:dd:4c:23:f5:c4:9b:97:91:0d:49:
             58:17:4b:af:80:35:69:2b:15:5b:d3:f5:f3:67:2d:d5:95:7d:
             8a:9b:0b:42:8e:9f:fb:d8:90:ce:86:75:ba:ca:a3:d9:c3:14:
             83:7a:ac:b7:4d:be:75:fa:4e:8a:f8:35:46:8c:47:b1:97:57:
             f2:25:6d:8d:9d:49:5a:34:e6:e0:b0:0f:19:56:44:ea:ef:8a:
             94:25:64:2a:28:18:2c:34:0f:71:98:de:1c:21:da:b2:99:c9:
             9e:40:69:16

    step ca init

✔ Deployment Type: Standalone
What would you like to name your new PKI?
✔ (e.g. Smallstep): Smallstep
What DNS names or IP addresses would you like to add to your new CA?
✔ (e.g. ca.smallstep.com[,1.1.1.1,etc.]): ca.internal.net,127.0.0.1
What IP and port will your new CA bind to?
✔ (e.g. :443 or 127.0.0.1:4343): 127.0.0.1:4343
What would you like to name the CA&rsquo;s first provisioner?
✔ (e.g. you@smallstep.com): evan@evan.app
Choose a password for your CA keys and first provisioner.
✔ [leave empty and we&rsquo;ll generate one]:

Generating root certificate&#x2026; done!
Generating intermediate certificate&#x2026; done!

✔ Root certificate: *Users/ejohnson1*.step/certs/root<sub>ca.crt</sub>
✔ Root private key: *Users/ejohnson1*.step/secrets/root<sub>ca</sub><sub>key</sub>
✔ Root fingerprint: 123ae7987c0dbdea85d0e9533ebab8675115cda056a669fbac3adcac03d71d5c
✔ Intermediate certificate: *Users/ejohnson1*.step/certs/intermediate<sub>ca.crt</sub>
✔ Intermediate private key: *Users/ejohnson1*.step/secrets/intermediate<sub>ca</sub><sub>key</sub>
✔ Database folder: *Users/ejohnson1*.step/db
✔ Default configuration: *Users/ejohnson1*.step/config/defaults.json
✔ Certificate Authority configuration: *Users/ejohnson1*.step/config/ca.json

Your PKI is ready to go. To generate certificates for individual services see &rsquo;step help ca&rsquo;.

FEEDBACK 😍 🍻
  The step utility is not instrumented for usage statistics. It does not phone
  home. But your feedback is extremely valuable. Any information you can provide
  regarding how you’re using \`step\` helps. Please send us a sentence or two,
  good or bad at feedback@smallstep.com or join GitHub Discussions
  <https://github.com/smallstep/certificates/discussions> and our Discord
  <https://u.step.sm/discord>.

Start the CA

    step-ca $(step path)/config/ca.json


<a id="org6c08720"></a>

### Set up certificates

    step ca certificate "example.internal.net" server.crt server.key

✔ Provisioner: evan@evan.app (JWK) [kid: QNInyqJrYcvNZe9xrMgXb7f4XKKZEBtWXiCUsX2sgcs]
✔ Please enter the password to decrypt the provisioner key:
✔ CA: <https://localhost:14343>
✔ Certificate: server.crt
✔ Private Key: server.key

Get a copy of the root CA certs

    step ca root ca.crt

The root certificate has been saved in ca.crt.


<a id="orgdc174bc"></a>

## Server component

We&rsquo;ll start a server and have it use the server certificates. Note that this does not need to be an HTTPS server. The `tls` component could be used instead to encapsulate any protocol. HTTPS uses `tls` and `http` under the hood.

In this example we&rsquo;re not validating the client certificate.

    const fs = require('fs');
    const https = require('https');
    const cert = fs.readFileSync('server.crt');
    const key = fs.readFileSync('server.key');
    const options = { cert, key }
    const handler = (req, res) => {
        res.writeHead(200);
        res.end('Hello, world!');
    };
    
    https.createServer(options, handler).listen(9443);

If we want to validate the client, we&rsquo;ll include CA and request the certificate from the client for verification. We&rsquo;ll handled the rejection in the handler rather than allowing the server component to reject them.

    const fs = require('fs');
    const https = require('https');
    const requestCert = true;
    const rejectUnauthorized = false;
    const ca = fs.readFileSync('ca.crt');
    const cert = fs.readFileSync('server.crt');
    const key = fs.readFileSync('server.key');
    const options = { requestCert, rejectUnauthorized, ca, cert, key };
    const handler = (req, res) => {
        if(!req.client.authorized) {
            res.writeHead(401);
            return res.end('Invalid client certificate');
        }
        res.writeHead(200);
        res.end('Hello, world!');
    };
    
    https.createServer(options, handler).listen(9443);


<a id="org80024cf"></a>

## Client component

On the client side we&rsquo;ll want to generate a certificate for it to use.

    step ca certificate "client" client.crt client.key

We should already have the ca.crt available for use on the client, but it is needed as well.

    const fs = require('fs')
    ;
    const https = require('https');
    const { hostname, port, path } = new URL('https://localhost:9443/');
    const method = 'GET';
    const ca = fs.readFileSync('ca.crt');
    const cert = fs.readFileSync('client.crt');
    const key = fs.readFileSync('client.key');
    const options = { hostname, port, path, method, ca, cert, key };
    const handler = (res) => {
        res.on('data', (data) => process.stdout.write(data));
    };
    const req = https.request(options, handler);
    
    req.end();


<a id="org895b26a"></a>

## Demonstrating mutual auth

Now with the `server-ca.js` and the `client-ca.js` we can run them together to prove they both work. The client won&rsquo;t connect if the server&rsquo;s certificate is invalid by default, and with our server code, it will validate the client is who they say they are using certificate auth.

Start the server:

    node server-ca.js

Run the client:

    node client-ca.js

Our server properly responds with `Hello, world!` and all certificates are valid.


<a id="org11a4a4b"></a>

## Testing no client certificate

To test without a client cert, we modify the client to not send the certificate, and use the system default.

    const fs = require('fs')
    ;
    const https = require('https');
    const { hostname, port, path } = new URL('https://localhost:9443/');
    const method = 'GET';
    const options = { hostname, port, path, method };
    const handler = (res) => {
        res.on('data', (data) => process.stdout.write(data));
    };
    const req = https.request(options, handler);
    
    req.end();

    node client-noca.js

node:events:346
      throw er; // Unhandled &rsquo;error&rsquo; event
      ^

Error: unable to get local issuer certificate
    at TLSSocket.onConnectSecure (node:<sub>tls</sub><sub>wrap</sub>:1531:34)
    at TLSSocket.emit (node:events:369:20)
    at TLSSocket.<sub>finishInit</sub> (node:<sub>tls</sub><sub>wrap</sub>:945:8)
    at TLSWrap.ssl.onhandshakedone (node:<sub>tls</sub><sub>wrap</sub>:719:12)
Emitted &rsquo;error&rsquo; event on ClientRequest instance at:
    at TLSSocket.socketErrorListener (node:<sub>http</sub><sub>client</sub>:469:9)
    at TLSSocket.emit (node:events:369:20)
    at emitErrorNT (node:internal/streams/destroy:188:8)
    at emitErrorCloseNT (node:internal/streams/destroy:153:3)
    at processTicksAndRejections (node:internal/process/task<sub>queues</sub>:81:21) {
  code: &rsquo;UNABLE<sub>TO</sub><sub>GET</sub><sub>ISSUER</sub><sub>CERT</sub><sub>LOCALLY</sub>&rsquo;
}

The client will not connect because it does not have the CA of the server. We&rsquo;ll modify the code so it **does** have the CA, but does not send a client cert:

    const fs = require('fs')
    ;
    const https = require('https');
    const ca = fs.readFileSync('ca.crt');
    const { hostname, port, path } = new URL('https://localhost:9443/');
    const method = 'GET';
    const options = { hostname, port, path, method, ca };
    const handler = (res) => {
        res.on('data', (data) => process.stdout.write(data));
    };
    const req = https.request(options, handler);
    
    req.end();

    node client-nocert.js

We connect successfully, but the server rejects the certificate from the system since it was not signed by our CA.

This fully demonstrates TLS and HTTPS&rsquo;s ability to verify both server and client certificates through mutual authentication.

