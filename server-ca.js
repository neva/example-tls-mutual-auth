const fs = require('fs');
const https = require('https');
const requestCert = true;
const rejectUnauthorized = false;
const ca = fs.readFileSync('ca.crt');
const cert = fs.readFileSync('server.crt');
const key = fs.readFileSync('server.key');
const options = { requestCert, rejectUnauthorized, ca, cert, key };
const handler = (req, res) => {
    if(!req.client.authorized) {
        res.writeHead(401);
        return res.end('Invalid client certificate');
    }
    res.writeHead(200);
    res.end('Hello, world!');
};

https.createServer(options, handler).listen(9443);
