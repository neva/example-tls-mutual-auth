const fs = require('fs');
const https = require('https');
const { hostname, port, path } = new URL('https://localhost:9443/');
const method = 'GET';
const options = { hostname, port, path, method };
const handler = (res) => {
    res.on('data', (data) => process.stdout.write(data));
};
const req = https.request(options, handler);

req.end();
