const fs = require('fs');
const https = require('https');
const cert = fs.readFileSync('server.crt');
const key = fs.readFileSync('server.key');
const options = { cert, key }
const handler = (req, res) => {
    res.writeHead(200);
    res.end('Hello, world!');
};

https.createServer(options, handler).listen(9443);
